﻿#pragma strict

var Health = 200;

function ApplyDamage (TheDammage : int)
{
	Health -= TheDammage;
	if(Health <= 0)
	{
		Dead();
	}
}

function Dead()
{
	Debug.Log("Player Died");
	Destroy (gameObject);
}